package rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;


/**
 * Created by saviola on 5/15/16.
 */
@ApplicationPath("rest")
public class ApplicationConfig extends Application {
}
